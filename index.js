//importo express
const express = require('express');
//iniziallizzo express e ne assegna il valore del framework 
const app = express();
//includo mongodb client library
const MongoDb = require('mongodb').MongoClient; 

//dichiaro i parametri per connettersi al server e al db MONGO

const mongoDbUrl = 'mongodb://localhost:27017'
const mongoDbDataBaseCars = 'rest-api-cars';

const writeBrandtoDb = (req,res) => {
	
	MongoDb.connect(mongoDbUrl, (err,client)=> {
		const db = client.db(mongoDbDataBaseCars);
		const collection = db.collection('cars');

		var myCarBrand = req.query.car;
		var myCar = {car: myCarBrand, model: 'sport' };

		collection.insert(myCar,(err,result)=> {
			res.send('Object cretaed:' +myCar);
		});

		client.close();
	}); 

}
const readCarsFromDb = (req,res) => {

		MongoDb.connect(mongoDbUrl, (err,client)=> {
			const db = client.db(mongoDbDataBaseCars);
			const collection = db.collection('cars');

			collection.find({}).toArray((err,docs) => {
				res.send(JSON.stringify(docs));
			});

			client.close();
		}); 
} 

const readCarFromDb = (req,res) => {

		MongoDb.connect(mongoDbUrl, (err,client)=> {
			const db = client.db(mongoDbDataBaseCars);
			const collection = db.collection('cars');

			var carToSearch = req.query.car;

			collection.find({car:carToSearch}).toArray((err,docs) => {
				res.send(JSON.stringify(docs));
			});

			client.close();
		}); 
}


app.get('/writeBrand', writeBrandtoDb);
app.get('/readCars', readCarsFromDb);
app.get('/readCar', readCarFromDb);

// creo una rotta
app.get('/', (req,res)=> {
	var input = req.query.car;
	res.send('La marca della macchina è' + input);
});

//express deve ascolatre un server

app.listen(3000, () => 
	{console.log('server ready')
});

//chiamando una specifica rotta riusciamo a scrivere sul DB, 
//uno riceviamo un dato
//due scriviamo il dato su db
//tre comunichiamo al client che abbiamo effettuato l'operazione e con che risultato
